class NotThirdDimension(Exception):
    pass


class Keeper:

    # constructor, requires a 3-D array
    def __init__(self, third_dim):
        self.x = []
        self.y = []
        if third_dim.ndim == 3:
            for row in third_dim:
                for col in row:
                    self.x.append(col[0])
                    self.y.append(col[1])
        else:
            raise NotThirdDimension("Entered array was not three dimensional")

    def empty(self):
        self.x = []
        self.y = []
