import cv2
from retro_detector import VisionPipeline
from threading import Thread
import time
from networktables import NetworkTables


class Runner(Thread):

    def __init__(self, cam_id):
        """
        Creates a thread that can process video from a specific camera
        :param cam_id: ID of camera
        """
        Thread.__init__(self)
        self.cam_id = cam_id

    def run(self):
        vid = cv2.VideoCapture(self.cam_id)
        v = VisionPipeline()
        NetworkTables.initialize(server='roborio-219-frc.local')
        smart_dash = NetworkTables.getTable('SmartDashboard')
        debug = False
        if debug:
            n = 1
            sum_time = 0
        while True:
            if debug:
                start_time = time.time()
            ret, frame = vid.read()
            angle = v.process(frame)
            smart_dash.putNumber('visionAngle', angle)
            if debug:
                delta_time = time.time() - start_time
                sum_time += delta_time
                avg_time = sum_time/n
                print('took %.8f' % delta_time)
                print('average %.8f' % avg_time)
                n += 1
            time.sleep(0.2)

if __name__ == '__main__':
    vp = Runner(0)
    vp.setName('Vision 1')
    vp.start()
