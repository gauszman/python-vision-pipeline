import cv2
from keeper import Keeper


class VisionPipeline:

    def __init__(self):
        self.__hsv_threshold_input = None
        # Threshold
        self.__hsv_threshold_hue = [50, 120]
        self.__hsv_threshold_saturation = [140, 255]
        self.__hsv_threshold_value = [85, 255]
        self.hsv_threshold_output = None

        # Mask
        self.__mask_mask = self.hsv_threshold_output
        self.mask_output = None

        # Desaturate
        self.__desaturate_input = self.mask_output
        self.desaturate_output = None

        # Find Contours
        self.__find_contours_input = self.desaturate_output
        self.find_contours_output = None

        # Filter Contours
        self.__filter_contours_contours = self.find_contours_output
        self.__filter_contours_min_area = 500
        self.filter_contours_output = None

        # Approx Contours
        self.__approx_contours_contours = self.filter_contours_output
        self.approx_contours_output = None

        # Find Turning Angle
        self.__find_angle_input = self.approx_contours_output
        self.find_angle_output = None

    def process(self, source):
        """
        Take an image, filter and mask, and find contours
        """
        # HSV Threshold
        self.__hsv_threshold_input = source
        (self.hsv_threshold_output) = self.__hsv_threshold(self.__hsv_threshold_input, self.__hsv_threshold_hue,
                                                           self.__hsv_threshold_saturation, self.__hsv_threshold_value)

        # Mask
        self.__mask_input = source
        self.__mask_mask = self.hsv_threshold_output
        (self.mask_output) = self.__mask(self.__mask_input, self.__mask_mask)

        # Desaturate
        self.__desaturate_input = self.mask_output
        (self.desaturate_output) = self.__desaturate(self.__desaturate_input)

        # Find Contours
        self.__find_contours_input = self.desaturate_output
        (self.find_contours_output) = self.__find_contours(self.__find_contours_input)

        # Filter Contours
        self.__filter_contours_contours = self.find_contours_output
        (self.filter_contours_output) = self.__filter_contours(self.__filter_contours_contours,
                                                               self.__filter_contours_min_area)

        # Approx Contours
        self.__approx_contours_contours = self.filter_contours_output
        (self.approx_contours_output) = self.__approx_contours(self.__approx_contours_contours)

        # Find Turning Angle
        self.__find_angle_input = self.approx_contours_output
        (self.find_angle_output) = self.__find_angle(self.__find_angle_input, 240, 8)

        return self.find_angle_output


    @staticmethod
    def __hsv_threshold(source, hue, sat, val):
        """
        Performs an HSV Threshold on a source image
        :param source: source image
        :param hue: array of hue values to threshold
        :param sat: array of saturation values to threshold
        :param val: array of value values to threshold
        :return: the source image thresholded
        """
        out = cv2.cvtColor(source, cv2.COLOR_BGR2HSV)
        return cv2.inRange(out,(hue[0], sat[0], val[0]), (hue[1], sat[1], val[1]))

    @staticmethod
    def __mask(source, mask):
        """
        Performs a mask on a source image
        :param source: source image
        :param mask: mask to use on source image
        :return: the source image masked
        """
        return cv2.bitwise_and(source, source, mask=mask)

    @staticmethod
    def __desaturate(source):
        """
        Performs a desaturation on a source image
        :param source: source image
        :return: the source image desaturated
        """
        return cv2.cvtColor(source, cv2.COLOR_BGR2GRAY)

    @staticmethod
    def __find_contours(source):
        """
        Finds all contours within the source image
        :param source: source image
        :return: contours within the image
        """
        mode = cv2.RETR_TREE
        method = cv2.CHAIN_APPROX_SIMPLE
        im2, contours, hierarchy = cv2.findContours(source, mode=mode, method=method)
        return contours

    @staticmethod
    def __filter_contours(input_contours, min_area):
        """
        Filters the contours based on certain parameters
        :param input_contours: the contours to filter
        :param min_area: the minimum area of each contour
        :return: filtered contours within image
        """
        output = []
        for contour in input_contours:
            area = cv2.contourArea(contour)
            if area < min_area:
                continue

            output.append(contour)
        return output

    @staticmethod
    def __approx_contours(input_contours):
        """
        Approximates each contour to reduce amount of points
        :param input_contours: the contours to approximate
        :return: approximated contours
        """
        output = []
        kp = None
        for contour in input_contours:
            error = 0.1*cv2.arcLength(contour, True)
            approx = cv2.approxPolyDP(contour, error, True)
            kp = Keeper(approx)
        if kp is None:
            return output
        for x, y in zip(kp.x, kp.y):
            output.append((x, y))
        return output

    @staticmethod
    def __find_angle(coordinates, center_line_x, px_per_deg):
        """
        Finds the angle to turn based
        :param coordinates: array of coordinate points to average
        :param center_line_x: the center line of your image
        :param px_per_deg: how many pixels per degree your camera is (camera_width/horizontal_field_of_view)
        :return: the angle to turn
        """
        if len(coordinates) == 0:
            return 0
        sumX = 0
        for points in coordinates:
            sumX += points[0]
        avgX = sumX/len(coordinates)
        angle = (avgX - center_line_x) / px_per_deg
        return angle
