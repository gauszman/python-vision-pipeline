class Point:
    _point = ()

    def __init__(self, x, y):
        self._point = x, y

    def __str__(self):
        return str(self._point)

    # return X coord
    def getX(self):
        return self._point[0]

    # return Y coord
    def getY(self):
        return self._point[1]
